package model

trait Figure
object Figure {
  val all = Set(
    King, Rook, Queen, Bishop, Knight
  )
}

case object King extends Figure
case object Rook extends Figure
case object Queen extends Figure
case object Bishop extends Figure
case object Knight extends Figure

