package model

import services.PeacefulPositionsService

object ChessChallenge {

  def main (args: Array[String]): Unit = {
    val startTime = System.nanoTime()

    val result = PeacefulPositionsService.peacefulPositions(
      BoardDimensions(7,7),
      Map(
        King -> 2,
        Queen ->2,
        Bishop ->2,
        Knight ->1
      )
    )

    val endTime = System.nanoTime()
    result.foreach(x => println(x.toString))

    println(result.size + " peaceful positions were found in " + (endTime - startTime)/1000000000.0 + " seconds")
  }
}
