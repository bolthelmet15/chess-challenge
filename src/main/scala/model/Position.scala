package model

case class Position (figureToCells: Map[Figure, Set[Cell]])
