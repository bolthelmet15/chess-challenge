package model

case class BoardDimensions(rowsQty: Int, columnsQty: Int)
