package services

import model._

object CellsUnderAttackService {

  def cellsUnderAttack(boardDimensions: BoardDimensions, field: Field): Set[Cell] = {
    val cell = field.cell
    field.figure match {
      case King => cellsUnderKingAttack(boardDimensions, cell)
      case Bishop => cellsUnderBishopAttack(boardDimensions, cell)
      case Knight => cellsUnderKnightAttack(boardDimensions, cell)
      case Queen => cellsUnderQueenAttack(boardDimensions, cell)
      case Rook => cellsUnderRookAttack(boardDimensions, cell)
    }
  }

  private def cellsUnderKingAttack(board: BoardDimensions, cell: Cell) = {
    val rowIndex = cell.rowNumber
    val colIndex = cell.columnNumber
    val s = Set(
      Cell(rowIndex-1, colIndex-1),
      Cell(rowIndex-1, colIndex),
      Cell(rowIndex-1, colIndex+1),
      Cell(rowIndex, colIndex-1),
      Cell(rowIndex, colIndex+1),
      Cell(rowIndex+1, colIndex-1),
      Cell(rowIndex+1, colIndex),
      Cell(rowIndex+1, colIndex+1)
    )
     s.filter(cellIsInsideBoard(board))
  }

  private def cellsUnderBishopAttack(board: BoardDimensions, cell: Cell) = {
    val leftGap = cell.columnNumber-1
    val rightGap = board.columnsQty - cell.columnNumber
    val topGap = cell.rowNumber-1
    val bottomGap = board.columnsQty - cell.rowNumber

    val cellsToLeftTop = (1 to Math.min(leftGap, topGap)).map { i =>
      Cell(
        cell.rowNumber-i,
        cell.columnNumber -i
      )
    }.toSet

    val cellsToRightTop = (1 to Math.min(rightGap, topGap)).map { i =>
      Cell(
        cell.rowNumber-i,
        cell.columnNumber +i
      )
    }.toSet

    val cellsToLeftBottom = (1 to Math.min(leftGap, bottomGap)).map { i =>
      Cell(
        cell.rowNumber+i,
        cell.columnNumber -i
      )
    }.toSet

    val cellsToRightBottom = (1 to Math.min(rightGap, bottomGap)).map { i =>
      Cell(
        cell.rowNumber+i,
        cell.columnNumber +i
      )
    }.toSet

    cellsToLeftBottom ++ cellsToLeftTop ++ cellsToRightBottom ++ cellsToRightTop

  }

  private def cellsUnderKnightAttack(board: BoardDimensions, cell: Cell) = {
    val rowIndex = cell.rowNumber
    val colIndex = cell.columnNumber
    val s = Set(
      Cell(rowIndex-2, colIndex-1),
      Cell(rowIndex-2, colIndex+1),
      Cell(rowIndex-1, colIndex+2),
      Cell(rowIndex-1, colIndex-2),
      Cell(rowIndex+1, colIndex+2),
      Cell(rowIndex+1, colIndex-2),
      Cell(rowIndex+2, colIndex-1),
      Cell(rowIndex+2, colIndex+1)
    )
    s.filter(cellIsInsideBoard(board))
  }

  private def cellsUnderQueenAttack(board: BoardDimensions, cell: Cell) = {
    val fromRook = cellsUnderRookAttack(board,cell)
    val fromBishop = cellsUnderBishopAttack(board, cell)
    cellsUnderBishopAttack(board, cell) ++ cellsUnderRookAttack(board,cell)
  }

  private def cellsUnderRookAttack(board: BoardDimensions, cell: Cell) = {
    val rowsHitOnTop = 1 until cell.rowNumber
    val rowsHitOnBottom = cell.rowNumber + 1 to board.rowsQty
    val verticalHits = (rowsHitOnTop++rowsHitOnBottom).map(Cell(_, cell.columnNumber)).toSet

    val columnsHitOnLeft = 1 until cell.columnNumber
    val columnsHitOnRight = cell.columnNumber + 1 to board.columnsQty

    val horizontalHits = (columnsHitOnLeft++columnsHitOnRight).map(Cell(cell.rowNumber, _))
    verticalHits ++ horizontalHits
  }

  private def cellIsInsideBoard(board: BoardDimensions)(cell: Cell) =
    cell.rowNumber <= board.rowsQty &&
      cell.columnNumber <= board.columnsQty &&
      cell.rowNumber > 0 &&
      cell.columnNumber > 0

}
