package services

import model._

import scala.collection.immutable.Set

object PeacefulPositionsService {

  import CellsUnderAttackService._

  def peacefulPositions(board: BoardDimensions, figureToQty: Map[Figure, Int]): Set[Position] = {

    def loop(acc: Set[PositionWithPeacefulCellsLeft], figuresLeft: List[Figure]): Set[Position] = {
      figuresLeft match {
        case Nil => acc.map(_.position)
        case head::tail => {
          val newAcc = acc.flatMap(peacefulPositionsAfterAddingFigure(_, head))
          loop(newAcc, tail)
        }
      }
    }

    val figuresList = figureToQty.flatMap{
      case (fig, qty) => {
        (1 to qty).map(_ => fig)
      }
    }.toList

    val initialPositionWithPeacefulCells = PositionWithPeacefulCellsLeft(
      board,
      Position(Map.empty),
      allBoardCells(board)
    )

    loop(
      Set(
        initialPositionWithPeacefulCells
      ),
      figuresList
    )
  }

  private def peacefulPositionsAfterAddingFigure(positionWithPeacefulCellsLeft: PositionWithPeacefulCellsLeft, figure: Figure): Set[PositionWithPeacefulCellsLeft] = {
    import positionWithPeacefulCellsLeft._

    val peaceFullCellToPlacesHitWithAddedFigure: Map[Cell, Set[Cell]] = peacefulCells.map(cell => (cell -> cellsUnderAttack(board, Field(cell, figure)))).toMap

    def figureCanBePlacedInCellPeacefuly(cell: Cell) = {
      import positionWithPeacefulCellsLeft._
      val cellsHit = peaceFullCellToPlacesHitWithAddedFigure(cell)
      val alreadyTakenCells = position.figureToCells.values.flatten.toSet
      val newFigureDoesNotHitOldOnes = cellsHit.forall( x => !alreadyTakenCells.contains(x))
      newFigureDoesNotHitOldOnes
    }

    val placesForPeacefulFigure = peacefulCells.filter(figureCanBePlacedInCellPeacefuly)

    def positionWithPeacefulCellsLeftAfterAddingFigure(cell: Cell) = {
      val cellsHit = peaceFullCellToPlacesHitWithAddedFigure(cell)
      val positionWithNewFigure = addFigureCellToPosition(position, cell, figure)
      PositionWithPeacefulCellsLeft(
        board,
        positionWithNewFigure,
        positionWithPeacefulCellsLeft.peacefulCells -- cellsHit - cell
      )
    }

    placesForPeacefulFigure.map(positionWithPeacefulCellsLeftAfterAddingFigure)
  }

  private def addFigureCellToPosition(position:Position, cell:Cell, figure:Figure) = {
    val updatedFigureCells = position.figureToCells.get(figure).getOrElse(Set.empty) + cell
    Position(
      position.figureToCells.updated(figure, updatedFigureCells)
    )
  }

  private def allBoardCells(board: BoardDimensions) = (for {
    row <- 1 to board.rowsQty
    col <- 1 to board.columnsQty
  } yield Cell(row,col)).toSet
}

case class PositionWithPeacefulCellsLeft(board: BoardDimensions, position: Position, peacefulCells:Set[Cell])
