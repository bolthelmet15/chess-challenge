package services

import model.Field
import org.scalatest.{Matchers, FunSuite}

class CellsUnderAttackTestExecutor extends FunSuite with Matchers{

  import CellsUnderAttackService._

  //todo: separate tests, add diagnostics
  test("All test cases should be executed"){

    CellsUnderAttackTestCases.inputs.foreach{ inputData =>

      cellsUnderAttack(
        inputData.board,
        Field(
          inputData.cell,
          inputData.figure
        )
      ) should contain theSameElementsAs(inputData.expectedResult)

    }

  }

}
