package services

import model._

case class CellsUnderAttackTestInput(
                      board: BoardDimensions,
                      figure: Figure,
                      cell: Cell,
                      expectedResult: Set[Cell]
                      )

object CellsUnderAttackTestCases {
  val threeOnThree = BoardDimensions(3, 3)


  val inputs = List (
    CellsUnderAttackTestInput(threeOnThree, Bishop, Cell(1,1), Set(Cell(2,2), Cell(3,3))),
    CellsUnderAttackTestInput(threeOnThree, Bishop, Cell(2,2), Set(Cell(1,1), Cell(3,3), Cell(1,3), Cell(3,1))),
    CellsUnderAttackTestInput(threeOnThree, Bishop, Cell(3,3), Set(Cell(1,1), Cell(2,2))),
    CellsUnderAttackTestInput(threeOnThree, Bishop, Cell(2,1), Set(Cell(1,2), Cell(3,2))),
    CellsUnderAttackTestInput(threeOnThree, Rook, Cell(1,1), Set(Cell(2,1), Cell(3,1), Cell(1,2), Cell(1,3))),
    CellsUnderAttackTestInput(threeOnThree, Rook, Cell(2,2), Set(Cell(1,2), Cell(3,2), Cell(2,1), Cell(2,3))),
    CellsUnderAttackTestInput(threeOnThree, Rook, Cell(3,3), Set(Cell(3,1), Cell(3,2), Cell(1,3), Cell(2,3))),
    CellsUnderAttackTestInput(threeOnThree, Rook, Cell(2,1), Set(Cell(1,1), Cell(3,1), Cell(2,2), Cell(2,3))),
    CellsUnderAttackTestInput(threeOnThree, King, Cell(1,1), Set(Cell(1,2), Cell(2,2), Cell(2,1))),
    CellsUnderAttackTestInput(threeOnThree, King, Cell(2,2), Set(Cell(1,1), Cell(1,2), Cell(1,3), Cell(2,1), Cell(2,3), Cell(3,1), Cell(3,2), Cell(3,3))),
    CellsUnderAttackTestInput(threeOnThree, King, Cell(2,1), Set(Cell(1,1), Cell(1,2), Cell(2,2), Cell(3,2), Cell(3, 1))),
    CellsUnderAttackTestInput(threeOnThree, Queen, Cell(1,1), Set(Cell(2,2), Cell(3,3), Cell(1,2), Cell(1,3), Cell(2,1), Cell(3,1))),
    CellsUnderAttackTestInput(threeOnThree, Queen, Cell(2,2), Set(Cell(1,1), Cell(1,2), Cell(1,3), Cell(2,1), Cell(2,3), Cell(3,1), Cell(3,2), Cell(3,3))),
    CellsUnderAttackTestInput(threeOnThree, Queen, Cell(2,1), Set(Cell(1,1), Cell(1,2), Cell(2,3), Cell(3,1), Cell(3,2), Cell(2,2))),
    CellsUnderAttackTestInput(threeOnThree, Knight, Cell(1,1), Set(Cell(2,3), Cell(3,2))),
    CellsUnderAttackTestInput(threeOnThree, Knight, Cell(2,2), Set()),
    CellsUnderAttackTestInput(threeOnThree, Knight, Cell(2,1), Set(Cell(1,3), Cell(3,3))),
    CellsUnderAttackTestInput(threeOnThree, Knight, Cell(3,3), Set(Cell(1,2), Cell(2,1))),
    CellsUnderAttackTestInput(threeOnThree, Knight, Cell(3,2), Set(Cell(1,1), Cell(1,3)))
  )
}