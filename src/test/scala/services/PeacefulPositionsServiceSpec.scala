package services

import model._
import org.scalatest.{Matchers, FunSuite}


class PeacefulPositionsServiceSpec extends FunSuite with Matchers{


  test("3 on 3 board containing 2 Kings and 1 Rook should return positions when kings are on knight attack from rook") {
    val threeOnThree = BoardDimensions(3,3)

    val peacefulPosition1 = Position(
      Map(
        King -> Set(Cell(1,1), Cell(1,3)),
        Rook -> Set(Cell(3,2))
      )
    )

    val peacefulPosition2 = Position(
      Map(
        King -> Set(Cell(1,1), Cell(3,1)),
        Rook -> Set(Cell(2,3))
      )
    )

    val peacefulPosition3 = Position(
      Map(
        King -> Set(Cell(1,3), Cell(3,3)),
        Rook -> Set(Cell(2,1))
      )
    )

    val peacefulPosition4 = Position(
      Map(
        King -> Set(Cell(3,1), Cell(3,3)),
        Rook -> Set(Cell(1,2))
      )
    )

    val expected = Set(
      peacefulPosition1,
      peacefulPosition2,
      peacefulPosition3,
      peacefulPosition4
    )


    val result = PeacefulPositionsService.peacefulPositions(threeOnThree, Map(King -> 2, Rook -> 1))

    assertThatPositionsSetsMatch(result, expected)

  }

  test("4 on 4 board containing 2 Rooks and 2 Knights should return positions where knights are in a 3x3 square") {

    val fourOnFour = BoardDimensions(4,4)

    val peacefulPosition1 = Position(
      Map(
        Knight -> Set(Cell(1,2), Cell(1,4), Cell(3,2), Cell(3,4)),
        Rook -> Set(Cell(4,1), Cell(2,3))
      )
    )

    val peacefulPosition2 = Position(
      Map(
        Knight -> Set(Cell(1,2), Cell(1,4), Cell(3,2), Cell(3,4)),
        Rook -> Set(Cell(2,1), Cell(4,3))
      )
    )

    val peacefulPosition3 = Position(
      Map(
        Knight -> Set(Cell(2,2), Cell(2,4), Cell(4,2), Cell(4,4)),
        Rook -> Set(Cell(1,1), Cell(3,3))
      )
    )

    val peacefulPosition4 = Position(
      Map(
        Knight -> Set(Cell(2,2), Cell(2,4), Cell(4,2), Cell(4,4)),
        Rook -> Set(Cell(1,3), Cell(3,1))
      )
    )

    val peacefulPosition5 = Position(
      Map(
        Knight -> Set(Cell(2,1), Cell(4,1), Cell(2,3), Cell(4,3)),
        Rook -> Set(Cell(1,2), Cell(3,4))
      )
    )

    val peacefulPosition6 = Position(
      Map(
        Knight -> Set(Cell(2,1), Cell(4,1), Cell(2,3), Cell(4,3)),
        Rook -> Set(Cell(1,4), Cell(3,2))
      )
    )

    val peacefulPosition7 = Position(
      Map(
        Knight -> Set(Cell(1,1), Cell(3,1), Cell(3,3), Cell(1,3)),
        Rook -> Set(Cell(2,4), Cell(4,2))
      )
    )

    val peacefulPosition8 = Position(
      Map(
        Knight -> Set(Cell(1,1), Cell(3,1), Cell(3,3), Cell(1,3)),
        Rook -> Set(Cell(2,2), Cell(4,4))
      )
    )

    val expected = Set(
      peacefulPosition1,
      peacefulPosition2,
      peacefulPosition3,
      peacefulPosition4,
      peacefulPosition5,
      peacefulPosition6,
      peacefulPosition7,
      peacefulPosition8
    )


    val result = PeacefulPositionsService.peacefulPositions(fourOnFour, Map(Rook -> 2, Knight -> 4))

    assertThatPositionsSetsMatch(result, expected)

  }

  private def assertThatPositionsSetsMatch(left: Set[Position], right: Set[Position]) = {
    val leftPositionsAreContainedInRight = left.forall{ p =>
      right.find(positionsAreEquivalent(_,p)).nonEmpty
    }

    val rightPositionsAreContainedInLeft = right.forall{ p =>
      left.find(positionsAreEquivalent(_,p)).nonEmpty
    }

    assert{
      rightPositionsAreContainedInLeft && leftPositionsAreContainedInRight
    }
  }


  private def positionsAreEquivalent(left:Position, right: Position): Boolean ={
    Figure.all.forall{ figure =>
      val leftPosition = left.figureToCells.getOrElse(figure, Set.empty)

      val rightPosition = right.figureToCells.getOrElse(figure, Set.empty)

      leftPosition.forall(rightPosition.contains)
      rightPosition.forall(leftPosition.contains)
    }
  }

}
