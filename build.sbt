name := """chess-challenge"""

//mainClass in Compile := Some("HelloSlick")

scalaVersion := "2.11.7"

libraryDependencies ++= List(
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test"
)
